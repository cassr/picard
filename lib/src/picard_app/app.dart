@HtmlImport('app.html')
library picard.app;

import 'package:web_components/web_components.dart' show HtmlImport;
import 'package:polymer/polymer.dart';

@PolymerRegister('picard-app')
class PicardApp extends PolymerElement {
  PicardApp.created() : super.created() {
    polymerCreated();
  }
}
